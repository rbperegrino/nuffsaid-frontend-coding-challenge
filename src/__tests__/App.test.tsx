import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import App from '../App';
import '../Api';

jest.mock('../Api', () => ({
  __esModule: true,
  default: (callback: any) => callback({message: 'test', priority: 0})
}))

beforeEach(() => {
  render(<App />);
});


test('render App', () => {
  const comp = render(<App />);
  expect(comp).toBeTruthy();
})

test('render cards', async () => {
  const cards = await screen.findAllByRole('card');
  expect(cards).toBeTruthy();
  
})

test('remove one card', async () => {
  const card = await screen.findByRole('card');
  expect(card).toBeTruthy();
  const button = within(card).getByText('Clear');
  userEvent.click(button);
  expect(screen.queryByText(`${card.textContent}`)).toBeFalsy();

})



test('click stop button', async () => {
  const button = screen.getByTestId('stop-subscribe');
  expect(button.textContent).toBe('Stop');
  const cards = await screen.findAllByRole('card');
  expect(cards.length).toBeGreaterThanOrEqual(1);
  userEvent.click(button);
  const cardsStoped = await screen.findAllByRole('card');
  expect(button.textContent).toBe('Start');
  expect(cardsStoped.length).toBe(cards.length);
})

test('click clear button', async () => {
  const button = screen.getByTestId('clear-list');
  await userEvent.click(button);
  const cards = screen.queryAllByRole('card')
  expect(cards.length).toBe(0);
  expect(screen.getByTestId('count-1').textContent).toBe('Count 0');
  expect(screen.getByTestId('count-2').textContent).toBe('Count 0');
  expect(screen.getByTestId('count-3').textContent).toBe('Count 0');
})


test('notification snackbar', async () => {
  const card = await screen.findByTestId('card-0');
  const snackbar = screen.getByTestId('snackbar');
  expect(snackbar).toBeTruthy();
  expect(card.textContent).toContain(snackbar.textContent);
})
