import React, { useCallback, useState } from 'react';
import { useEffect } from 'react';
import generateMessage, { Message } from './Api';
import ErrorNotification from './components/ErrorNotification/ErrorNotification';
import Header from './components/Header/Header';
import  Lists from './components/Lists/Lists';
import { MessagesContext } from './contexts/MessagesContext';

 

const App: React.FC<{}> = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [subscribed, setSubscribed] = useState(true);
  const [notification, setNotification] = useState<Message | undefined>();

  const verifyIfIsSubscribedAndSetValues = useCallback((message: Message) => {
    if (subscribed) {
      setMessages(oldMessages => [...oldMessages, message]);
      if (message.priority === 0) {
        setNotification(message);
      }
    }
  },[setMessages, subscribed])

  useEffect(() => {
    const cleanUp = generateMessage((message: Message) => {
      message.timestamp = Date.now();
      verifyIfIsSubscribedAndSetValues(message);
    });
    return cleanUp;
  }, [verifyIfIsSubscribedAndSetValues]);

  

  const stopSubscribe = () => {
    setSubscribed(!subscribed);
  }

  const clearList = () => {
    setMessages([]);
  }

  const removeMessage = useCallback(
    (message) => {
      setMessages(messages.filter(msg => msg.message !== message))
      if (message === notification?.message) {
        setNotification(undefined);
      }
    },
    [messages, notification],
  )


  return (
      <div>
        <Header clearList={clearList} stopSubscribe={stopSubscribe} subscribed={subscribed}/>
        <MessagesContext.Provider value={{messages, removeMessage}}>
          <Lists />
        </MessagesContext.Provider>
        <ErrorNotification notification={notification} />
      </div>
  );
}

export default App;
