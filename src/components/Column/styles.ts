import { Grid } from '@material-ui/core';
import styled from 'styled-components';


export const GridStyled = styled(Grid)`
    padding: 8px;
    > h2 {
        margin: 2px 0;
    }
    > p {
        margin: 0 0 8px;
    }
`