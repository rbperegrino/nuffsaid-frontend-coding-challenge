import React , { memo, useEffect, useState} from 'react'; 
import { Message } from '../../Api';
import { useMessageContext } from '../../contexts/MessagesContext';
import  CardInfo  from '../Card/Card';
import { IPriority } from '../Lists/Lists';
import { GridStyled } from './styles';

export interface IColumnInfo {  
    priority: IPriority; 
}

const ColumnInfo: React.FC<IColumnInfo> = (props: IColumnInfo) => {
    const { priority } = props;
    const { messages: msgs } = useMessageContext();
    const [messages, setMessages] = useState<Message[]>();
    
    const title = `${priority.name} Type ${priority.id}`;

    useEffect(() => {
        const filterAndSort = msgs.filter(msg => msg.priority === priority.id - 1).sort((a, b) => b.timestamp! - a.timestamp!)
        setMessages(filterAndSort);
        return;
    }, [setMessages, msgs, priority.id])

    

    return <GridStyled item xs={4} data-testid={`error-column-${priority.id}`}>
        <h2>{title ? title : ''}</h2>
        <p data-testid={`count-${priority.id}`}>Count {messages?.length}</p>
        {messages?.map?.(msg => <CardInfo
            key={msg?.message}
            message={msg}
            color={priority.color}
        />)}
    </GridStyled>;
}

export default memo(ColumnInfo);