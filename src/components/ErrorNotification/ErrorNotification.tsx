import React, { useEffect, useState } from 'react';
import { Snackbar } from '@material-ui/core';
import { Message } from '../../Api';

export interface IErrorNotification {
    notification?: Message
}

const ErrorNotification: React.FC<IErrorNotification> = (props: IErrorNotification) => {
    const { notification } = props;
    
    const [showSnackbar, setShowsnackBar] = useState(false);
   

    useEffect(() => {
        setShowsnackBar(false)
        if (!notification || notification.priority > 0) return;
        setShowsnackBar(false)
        setShowsnackBar(true)
        setTimeout(() => setShowsnackBar(false), 2000);
        return;
    }, [setShowsnackBar, notification])

    const handleOnClose = () => {
        setShowsnackBar(false);
    }
    
    return <>
        {showSnackbar && <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={showSnackbar}
            message={notification?.message}
            onClose={handleOnClose}
            data-testid='snackbar'
        />}
    </>;
}

export default ErrorNotification;