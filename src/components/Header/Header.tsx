import React from 'react';
import { ButtonsContainer, ActionButton } from './styles';

interface IHeader {
    clearList: () => void;
    stopSubscribe: () => void;
    subscribed: boolean;
}
const Header: React.FC<IHeader> = (props: IHeader) => {
    const { clearList, stopSubscribe, subscribed } = props;
    return <>
        <h1 style={{ margin: 0}}>nuffsaid.com Coding Challenge</h1>
          <hr />
          <ButtonsContainer>
            <ActionButton
              variant="contained"
              color='inherit'
              onClick={stopSubscribe}
              data-testid='stop-subscribe'
            >
              {subscribed ? 'Stop' : 'Start'}
            </ActionButton>
            <ActionButton
              variant="contained"
              color='inherit'
              onClick={clearList}
              data-testid='clear-list'
            >
              Clear
            </ActionButton>
        </ButtonsContainer>
    </>
}

export default Header;