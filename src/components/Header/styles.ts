import { Button } from '@material-ui/core';
import styled from 'styled-components';

export const ButtonsContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    gap: 4px;
    margin-bottom: 80px;
    
`

export const ActionButton = styled(Button)`
    && {
        background-color: #88FCA3;
        font-weight: 600;
    }
`

