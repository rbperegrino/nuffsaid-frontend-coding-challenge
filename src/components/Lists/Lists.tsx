import { Grid } from '@material-ui/core';
import React from 'react';
import  ColumnInfo  from '../Column/Column';

export interface IPriority {
    id: number;
    name: string;
    color: string
}

const Lists: React.FC = () => {
   
    const prioritys: IPriority[] = [
        { id: 1, name: 'Error', color: '#F56236'},
        { id: 2, name: 'Warning', color: '#FCE788' },
        { id: 3, name: 'Info', color: '#88FCA3' }
    ];  
    
    return <Grid container spacing={16}>
        {prioritys.map((priority, index) => <ColumnInfo
            key={priority.name}
            priority={priority}
            />)}
    </Grid>
}

export default Lists;