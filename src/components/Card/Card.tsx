import React, { memo } from 'react'; 
import { Message } from '../../Api';
import { useMessageContext } from '../../contexts/MessagesContext';
import { CardActionsStyled, CardContentStyled, CardStyled, ClearButton } from './styles';

export interface ICardInfo {
    message: Message;
    color: string;
}

const CardInfo: React.FC<ICardInfo> = (props: ICardInfo) => {

    const { message, color } = props;
    const { removeMessage } = useMessageContext();

    return (
        
            <CardStyled role="card" bgcolor={color} data-testid={`card-${message.priority}`}>
                <CardContentStyled>
                    {message?.message}
                </CardContentStyled>
                <CardActionsStyled>
                    <ClearButton
                        onClick={() => removeMessage(message?.message)}
                    >
                    Clear</ClearButton>
                </CardActionsStyled>
            </CardStyled>
        
       
    )
}

export default memo(CardInfo);