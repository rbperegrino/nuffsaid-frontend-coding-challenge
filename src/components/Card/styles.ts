import { Button, CardContent, Card, CardActions } from '@material-ui/core'
import styled from 'styled-components'

export const CardStyled = styled(Card) <{ bgcolor: string; }>`
   &&{
       background-color: ${props => props.bgcolor};
        margin-bottom: 16px;
    }
`

export const ClearButton = styled(Button)`
   &&{
       background: transparent;
       text-transform: capitalize;
       color: black;
    }
`

export const CardActionsStyled = styled(CardActions)`
    display: flex;
    justify-content: flex-end;
`

export const CardContentStyled = styled(CardContent)`
    &&{
        padding: 8px;
    }
`
