import { createContext, useContext } from 'react';
import { Message } from '../Api';

export interface IMessageContext {
    messages: Message[];
    removeMessage: (message: string) => void;
}

export const MessagesContext = createContext<IMessageContext>({
    messages: [],
    removeMessage: () => { },
})

export const useMessageContext = () => useContext(MessagesContext)